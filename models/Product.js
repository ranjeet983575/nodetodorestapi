const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description:{
        type:String
    },
    user:{
        type: String,
        required: true
    }
});
module.exports = mongoose.model("Product", productSchema);
