const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const auth = require('../middleware/userjwt');


//Create product
router.post("/", auth, async (req, res, next) => {
    try {
        const product = await Product.create(
            {
                title: req.body.title,
                description: req.body.description,
                user: req.user.id
            }
        );

        if (!product) {
            res.status(400).json({
                success: false,
                msg: "Something went wrong"
            });
        }

        res.status(200).json({
            success: true,
            msg: "Success"
        });
    } catch (e) {
        console.log(e);
        next(e);
    }
});

// Listing all products
router.get("/", auth, async (req, res, next) => {
    Product.find()
        .then(products => {
            res.send(products);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while getting list of products."
            });
        });
});

module.exports = router;
