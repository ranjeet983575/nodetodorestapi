const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const bcryptjs=require('bcryptjs');
const jwt=require('jsonwebtoken');

//Create user Signup
router.post("/register", async (req, res,next) => {
    console.log(req);
    console.log(req.body);
    const {name,email,password}=req.body;
    try {
        let user_exist=await User.findOne({email:email});
        if(user_exist)
        {
            res.json({
                success:false,
                message:"User Exists",
            });
        }

        let user=new User();
            user.name=name;
            user.email=email;

            const salt=await  bcryptjs.genSalt(10);
            user.password=await bcryptjs.hash(password,salt);

            let size=200;
            user.avatar="https://www.gravatar.com/avatar?s="+size+'&d=retro';

            await user.save();
            //generating jwt token
            const payload={
                user:{
                    id:user.id,
                    email:user.email,
                    username:user.name
                }
            }

            jwt.sign(payload,process.env.jwtUserSecrete,{
                expiresIn: 360000
            },(err,token)=>{
                if(err) throw err;

                res.status(200).json({
                    success:true,
                    token:token
                });
            });

    }catch(err){
        console.log(err);
    }
});

//Create User Login
router.post("/login", async (req, res,next) => {
    const {email, password} = req.body;
    try {
        let user=await User.findOne({
            email:email
        });
        if(!user)
        {
            res.status(400).json({
                success:true,
                msg:"User Not Found"
            });
        }
        const isMatch=await bcryptjs.compare(password,user.password);
        if(!isMatch)
        {
            res.status(400).json({
                success:true,
                msg:"User Not Found"
            });
        }
        //generating jwt token
        const payload={
            user:{
                id:user.id,
                email:user.email,
                username:user.name
            }
        }

        jwt.sign(payload,process.env.jwtUserSecrete,{
            expiresIn: 360000
        },(err,token)=>{
            if(err) throw err;

            res.status(200).json({
                success:true,
                token:token
            });
        });
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
