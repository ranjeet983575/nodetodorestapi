//creating server with express js
const express =require('express');
const colors=require('colors');
const morgan=require('morgan');
const dotenv=require('dotenv');
const mongoose = require("mongoose");

const app=express();
//for printing all client api  request in console
app.use(morgan('dev'));
app.use(express.json({}));
app.use(express.json({
    extended:true
}));
dotenv.config({
    path:'./config/config.env'
});

//db connection
const PORT=process.env.PORT || 5000
mongoose.connect(process.env.DATABASE_URL, { useUnifiedTopology: true, useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => console.log("Connected to database".green.underline.bold));
//end of DB connection

app.use('/api/auth',require('./routers/userRoute'));
app.use('/api/product',require('./routers/productRoute'));

app.listen(PORT,console.log(`Server iS Running On Port ${PORT}`.red.underline.bold));
