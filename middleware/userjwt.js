const jwt=require('jsonwebtoken');

module.exports =async function(req,res,next){
    const token=req.header('Authorization');
    if(!token) {
        return res.status(401).json({
            msg:'Authorization Token Not Found'
        });
    }
    try {
           await jwt.verify(token,process.env.jwtUserSecrete,(err,decode)=>{
               if(err){
                   console.log(token);
                   res.status(401).json({
                       msg:'Token Not Valid'
                   });
               }else{
                   req.user=decode.user;
                   next();
               }
            });
    }catch (e) {
        console.log(e);
        res.json(500).json({
            msg:'Server Error'
        });
    }
}
